package services

import javax.inject.Inject
import java.nio.file.Path

import akka.stream.scaladsl.FileIO
import akka.stream.scaladsl._

import play.api.Configuration
import play.api.http.ContentTypes
import play.api.libs.ws._
import play.api.mvc.MultipartFormData.FilePart

import scala.concurrent.{ ExecutionContext, Future }

/**
 * A wrapper for accessing the conversion service.
 * */
class ModelConverterService @Inject() (
  config: Configuration,
  ws: WSClient,
//  val controllerComponents: ControllerComponents,
  implicit val executionContext: ExecutionContext,
) {
  val convertibleModelExtensions = Set(/*".obj", ".fbx", ".collada"*/)

//  private val converterApiBaseUrl: String = config.getOptional[String]("converterAPIEndpoint").getOrElse("localhost:5022")
//  private val apiVersion = "v1"
//  private val convertModelMethod = "models"

//  private val convertModelEndpoint = List(converterApiBaseUrl, apiVersion, convertModelMethod).mkString("/")

//  def convertModelToGltf(fileToConvert: Path, filename: String) = {
//    ws.url(convertModelEndpoint)
//      .withMethod("POST")
//      .withBody(
//        Source(List/*[MultipartFormData.Part[Source[ByteString, Future[IOResult]]]]*/(
//          FilePart(
//            "file",
//            filename,
//            Option(ContentTypes.BINARY),
//            FileIO.fromPath(fileToConvert)
//          )
//        ))
//      )
//      .stream()
//      .map { response =>
//        // TODO save the file
//        response.bodyAsSource
//      }
//  }
}
