package controllers

import example.myapp.helloworld.grpc.{ GreeterService, HelloReply, HelloRequest }
import javax.inject.Inject
import play.api.mvc.{ AbstractController, ControllerComponents }
//import play.modules.reactivemongo.{ MongoController, ReactiveMongoApi, ReactiveMongoComponents }
//import reactivemongo.play.json.collection.JSONCollection
//import reactivemongo.play.json._
import collection._

import scala.concurrent.{ ExecutionContext, Future }
import play.api.libs.json._

class HelloWorldController @Inject() (
  components: ControllerComponents,
//  val reactiveMongoApi: ReactiveMongoApi
) extends AbstractController(components)
  with GreeterService
//  with MongoController with ReactiveMongoComponents
{
  private implicit def ec: ExecutionContext = components.executionContext

//  implicit val jsonFormat: OFormat[HelloRequest] = Json.format[HelloRequest]

//  private def namesCollection = database.map {
//    _.collection[JSONCollection]("names")
//  }

  override def sayHello(in: HelloRequest): Future[HelloReply] = {
    Future.successful(HelloReply(s"Hello, ${in.name}!"))
    //    for {
//      coll <- namesCollection
//      _ <- coll.insert(ordered = false).one(in)
//    } yield {
//      HelloReply(s"Hello, ${in.name}!")
//    }
  }
}
