package routers.grpc

import akka.actor.ActorSystem
import akka.stream.Materializer
import example.myapp.helloworld.grpc.{ AbstractGreeterServiceRouter, HelloReply, HelloRequest }
import javax.inject.Inject
import annotations.GRPCRouter
import controllers.HelloWorldController

import scala.concurrent.Future

@GRPCRouter
class HelloWorldRouter @Inject()(mat: Materializer, system: ActorSystem, controller: HelloWorldController)
  extends AbstractGreeterServiceRouter(mat, system) {

  // We need to inject a Materializer since it is required by the abstract
  // router. It can also be used to access the ExecutionContext if you need
  // to transform Futures. For example:
  //
  // private implicit val matExecutionContext = mat.executionContext
  // 
  // But at this example, this is not necessary.

  override def sayHello(in: HelloRequest): Future[HelloReply] = controller.sayHello(in)
}
