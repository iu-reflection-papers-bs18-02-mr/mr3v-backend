package routers

import javax.inject.Inject
import play.api.routing.Router.Routes
import play.api.routing.SimpleRouter
import play.grpc.internal.PlayRouter

import scala.collection.JavaConverters.asScalaSet

import util.routesMonoid
import cats.Monoid

class GRPCRouter @Inject() (grpcRouters: java.util.Set[PlayRouter]) extends SimpleRouter {
  override def routes: Routes =
    Monoid[Routes].combineAll(asScalaSet(grpcRouters).map { _.routes })
}
