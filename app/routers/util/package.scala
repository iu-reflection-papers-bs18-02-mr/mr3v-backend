package routers

import cats.Monoid
import play.api.routing.Router.Routes

package object util {
  implicit val routesMonoid: Monoid[Routes] = new Monoid[Routes] {
    override def empty: Routes = PartialFunction.empty

    override def combine(x: Routes, y: Routes): Routes = x.orElse(y)
  }
}
