# Cloning the repository

The repository has submodules, so add `--recurse-submodules` flag, when cloning the repository, or if you have already cloned it do the

```bash
git submodule init
git submodule update
```

# Running the application

## Docker

```shell script
cd ./docker
docker-compose up
```

This will start the application on the port `9000`, by default.

## JVM

To run the server on the machine [`sbt`](https://www.scala-sbt.org/) and Java 11 are required.

In the current directory run one of the following commands:

```shell script
# run in development mode (the server will be available only through HTTP)
sbt run
# or
# run in production mode (HTTP & HTTPS)
sbt runProd
```

The server will be listening to port `9000` for HTTP and `9443` by default.

### Some known issues

If an exception related to the `uploads/` folder is being thrown - creation of the folder in the root of the project will help. Also, there might be a need to provide it the corresponding access rights with `chmod`.

# Project structure (TODO)

# Configuration

The application is [configured as a normal Play! application](https://www.playframework.com/documentation/2.8.x/Configuration).

The `conf/` directory contains an `application.conf` file where the configuration is supplied. For better modularity, the default config is split into different files, so that each part can be configured independently of others (e. g. http is configured independently of the mongo connection). This is possible because the config file uses [HOCON syntax](https://www.playframework.com/documentation/2.8.x/ConfigFile#HOCON-Syntax).

## Configuration files

> All the paths in this section are relative to the `conf/include`

> Instead of providing a custom configuration file, some values can be changed via environment variables. The available environment variables will be listed for each file in a corresponding section.

### Main (`main.conf`)

Aggregates all the others configurations and by default is being imported into the root of `../application.conf`. If the root-level configuration needs to be changed, this is a place for the change.


### <a name="configuration-application"></a> Application (`application.conf`)

Custom settings of the application.

#### Custom configuration options

- `fileUploadDirectory` - specifies the path to the folder, which stores the uploaded files. The default is **`uploads/`**.

#### Environment variables

- `FILE_UPLOAD_DIR` - specifies the path to the folder, which stores the uploaded files.

### <a name="configuration-mongo"></a> Mongo (`mongo.conf`)

MongoDB settings, such as connection string.

#### Environment variables

- `MONGO_URI` - _mongodb_ connection string in the following format:

`mongodb://[username:password@]host1[:port1][,hostN[:portN]]*/dbName?option1=value1&option2=value2`

### HTTP (`http.conf`)

Settings for HTTP, such as bind address and port.

#### Environment variables

- `PLAY_HTTP_ADDRESS` - IP address for the HTTP server to listen to
- `PLAY_HTTP_PORT` - port number to listen to

> To disable HTTP, set the port (either with the config or the environment variable `PLAY_HTTP_PORT`) to `disabled`.

### HTTPS (`https.conf`)

> Only works, when `runProd` was used to run the app with `sbt`, or a flag `-Dhttps.port=<port_number>` was provided.

Settings for HTTPS, such as bind address and port.

#### Environment variables

- `PLAY_HTTPS_ADDRESS` - IP address for the HTTP server to listen to
- `PLAY_HTTPS_PORT` - port number to listen to

### Modules (`modules.conf`)

Modules for the _Play! Framework_.

### Secret (`secret.conf`)

[Application's secret](https://www.playframework.com/documentation/2.8.x/ApplicationSecret).

# FAQ

##### Q: How to run some initializing code, when application starts (for example, to create a folder that might not exist)?
A: The file `app/StartUpService` contains a singleton class `StartUpService`, which is eagerly instantiated by Guice in `app/Module` at the application startup. This means, that the code inside it will run, when application starts.
