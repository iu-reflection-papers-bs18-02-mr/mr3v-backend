// use mr3v
db.createUser(
  {
    user: "api",
    pwd: "api-backend-password",
    roles: [
      { role: "readWrite", db: "mr3v" }
    ]
  }
)
