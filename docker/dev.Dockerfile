FROM hseeberger/scala-sbt:graalvm-ce-19.3.0-java11_1.3.8_2.12.10

USER sbtuser

# Volume with the source code of the application
VOLUME /home/sbtuser/src

WORKDIR /home/sbtuser/src

ENTRYPOINT ["sbt"]

CMD ["run"]
